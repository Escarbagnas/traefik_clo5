# traefik_clo5

to run traefik:
    docker run -d \
      -v /var/run/docker.sock:/var/run/docker.sock \
      -v $PWD/traefik.toml:/traefik.toml \
      -v $PWD/acme.json:/acme.json \
      -p 8082:80 \
      -p 443:443 \
      -l traefik.frontend.rule=Host:localhost \
      -l traefik.port=8082 \
      --network web \
      --name traefik_clo5\
      traefik:v1.7.19-alpine

with Dockerfile:

docker run -d -p 8080:8080 -p 80:80 \
-v $PWD/traefik.toml:/etc/traefik/traefik.toml \
-v /var/run/docker.sock:/var/run/docker.sock \
traefik:v1.7





